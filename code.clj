;example 1
(use '(incanter core stats charts))

;generate a histogram
(view (histogram (sample-normal 1000)))

;define a plot of tangent
(def love-lisp-data (function-plot tan -10 10))

;save an image of the plot
(save love-lisp-data "neato.png")

;----------------------------------------------------;

;example 2

(use '(incanter core datasets charts io))

;create a function to retrieve weather data
(defn weather-for-month [month]
  (-> (format "https://www.wunderground.com/history/airport/KMKE/2016/%d/10/MonthlyHistory.html?format=1" month)
    (read-dataset :header true)))

;cycle through all months, conjoin the rows
(def data (->> (range 1 13) (map weather-for-month) (apply conj-rows)))

;view histogram of mean temperature
(view (histogram "Mean TemperatureF" :nbins 100 :data data))

;extract the month from the dataset
(defn month [date] (Integer/parseInt (second (.split date "-"))))

;group data by month
(def grouped-by-month
(->> (map (fn [date temp] {:month (month date) :temp temp})
          ($ "CST" data) ($ "Mean TemperatureF" data))
     to-dataset
     ($rollup :mean :temp :month)
     ($order :month :asc)))

;view line chart of monthly temps
(view (line-chart :month :temp :data grouped-by-month))

;----------------------------------------------------;

;example 3
;Poincaré's Baker - http://io9.gizmodo.com/the-legend-of-the-mathematician-and-the-baker-1542891198

(use '(incanter core charts datasets stats))
(require '[incanter.distributions :as d])

;create a function representing the honest baker, takes mean and standard deviation
(defn honest-baker [mean sd]
     (let [distribution (d/normal-distribution mean sd)]
     (repeatedly #(d/draw distribution))))

;macro to honest baker with 1000g mean, 30 standard deviation
(defn honest-baker-distribution []
     (-> (take 10000 (honest-baker 1000 30))
     (histogram :x-label "Honest baker"
     :nbins 25)
     (view)))

;create a function representing the dishonest baker
(defn dishonest-baker [mean sd]
      (let [distribution (d/normal-distribution mean sd)]
      (->> (repeatedly #(d/draw distribution))
      (partition 13)
      (map (partial apply max)))))

;macro to dishonest baker with 950g mean, 30 standard deviation
(defn dishonest-baker-distribution[]
      (-> (take 10000 (dishonest-baker 950 30))
      (histogram :x-label "Dishonest baker"
      :nbins 25)
      (view)))

;measure skew
(defn catching-the-baker []
  (let [weights (take 10000 (dishonest-baker 950 30))]
  {:mean (mean weights)
  :median (median weights)
  :skewness (skewness weights)}))